var http = require('http');
var url = require('url');
var zlib = require('zlib');
var stream = require('stream');
var parser = require('../pdbparser_module');
var fs = require('fs');
var staticDirname = './views/';

http.createServer(function (request, response) {
	var pathname = url.parse(request.url).pathname;
	var pathParts = pathname.split('/');
	switch(pathParts[1]) {
		case '':
			response.writeHead(200, {
				'Content-Type': 'text/html; utf-8',
				'Content-Encoding': 'gzip'
			});
			fs.createReadStream(staticDirname + 'index.html')
				.pipe(zlib.createGzip())
				.pipe(response);
			break;
		case 'render':
			var start = new Date();
			http.get('http://www.rcsb.org/pdb/files/' + pathParts[2] + '.pdb.gz')
				.on('response', function (res) {
					if (res.statusCode === 200) {
						response.writeHead(200, {
							'Content-Type': 'application/Int32LE',
							'Content-Encoding': 'gzip'
						});
						res.pipe(zlib.createGunzip())
							.pipe(new parser())
							.pipe(zlib.createGzip())
							.pipe(response);
						} else {
							response.writeHead(404);
							response.end();
						}
				})
				.on('error', function (err) {
					console.log('file doesn\'t exist');
				// need error handling
				// http response status code
					response.end();
				});
			break;
		case 'css':
			response.writeHead(200, {
				'Content-Type': 'text/css',
				'Content-Encoding': 'gzip'
			});
			fs.createReadStream(staticDirname + pathname)
				.pipe(zlib.createGzip())
				.pipe(response);
			break;
		case 'js':
			response.writeHead(200, {
				'Content-Type': 'application/javascript',
				'Content-Encoding': 'gzip'
			});
			fs.createReadStream(staticDirname + pathname)
				.pipe(zlib.createGzip())
				.pipe(response);
			break;
		case 'images':
			response.writeHead(200, {
				'Content-Type': 'image/svg+xml',
				'Content-Encoding': 'gzip'
			});
			fs.createReadStream(staticDirname + pathname)
				.pipe(zlib.createGzip())
				.pipe(response);
			break;
		default:
			console.log(pathname);
			// console.log(request.headers);
			response.writeHead(404);
			response.end();
	}
}).listen(3000);

process.on('error', function (err) {
	// need error handling
	console.log(err);
});