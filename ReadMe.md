proteins.3d.project
=====================

This is a SPA that allows to view proteins' structure in 3D.

Contributors
------------

* [Andrei Hameza](https://bitbucket.org/andrei_hameza)
* [Anton Kazmiarchuk](https://bitbucket.org/Anton_Kazmiarchuk)
* [Uladzislau Sakalou](https://bitbucket.org/Uladzislau_Sakalou)
* [Darya Urekina](https://bitbucket.org/Darya_Urekina)
* Igor Rumyancev