var util = require('util');
var Transform = require('stream').Transform;
// regexp pattern that finds numbers with leading whitespace
var NUMBERS_PATTERN_REGEX = /\s[-+]?[0-9]*\.?[0-9]+/g;

/**
 * Pdbparser
 * create Pdbparser transforming stream child class
 * with using the constructor of stream.Transform parent class
 * @constructor
 * @param {Object} options
 */

function Pdbparser(options) {
	Transform.call(this, options);
}


// Pdbparser class inherits from stream.Transform class
util.inherits(Pdbparser, Transform);

/**
 * implementation of _transform method for Pdbparser class
 * more here http://nodejs.org/api/stream.html#stream_class_stream_transform_1
 * @param {Buffer|String} data
 * @param {String} encoding
 * @param {Function} done
 */

Pdbparser.prototype._transform = function (data, encoding, done) {
	var arrLines, n;

	// add the last line of previous data chunk to current one
	if (this._lastLine) {
		data = this._lastLine + data;
	}

	// split data chunk into lines and save to array
	arrLines = data.toString().split('\n');
	n = arrLines.length;

	// save last line of every data chunk to prevent breaking lines
	this._lastLine = arrLines[n - 1];

	// parse remaining (n-1) lines of data chunk from arrLines
	arrLines.every(function (line, index) {

		// if string starts from "ATOM" or "HETATM" substrings
		// call parseCoords function and push the result x, y, z coordinates to Pdbparser output stream
		if ((line.substring(0, 4) === 'ATOM') || (line.substring(0, 6) === 'HETATM')) {
			this.push( parseCoords(line, 2, 3, 4) );
		}

		return index < (n - 2);

	}, this);

	done();
}

/**
 * implementation of _flush method for Pdbparser class
 * need to parse last line of the stream that was saved in _lastLine property
 * more here http://nodejs.org/api/stream.html#stream_transform_flush_callback
 * @param {Function} done
 */

Pdbparser.prototype._flush = function (done) {
	var line = this._lastLine;

	if ((line.substring(0, 4) === 'ATOM') || (line.substring(0, 6) === 'HETATM')) {
		this.push( parseCoords(line, 2, 3, 4) );
	}

	done();
};

/**
 * get coordinates x, y, z multiplied by 1000 from string
 * indexX, indexY, indexZ define positions of coordinates
 * in array that return String.prototype.match method
 * @param {String} str
 * @param {Number} indexX
 * @param {Number} indexY
 * @param {Number} indexZ
 * @returns {Buffer}
 */

function parseCoords(str, indexX, indexY, indexZ) {
	if (!str) {
		throw new TypeError('str parameter required');
	}

	if (typeof str !== 'string') {
		throw new TypeError('str must be a string')
	}

	// create 12-bytes buffer for 3 coordinates: x, y, z
	var buf = new Buffer(12);

	// search all numbers with leading whitespace using regular expression
	// /\s[-+]?[0-9]*\.?[0-9]+/g
	var arrFloats = str.match(NUMBERS_PATTERN_REGEX);

	// write results to buffer
	// Note: type of numbers in buffer is signed 32 bit integer with little endian order of bytes
	buf.writeInt32LE(~~(parseFloat(arrFloats[indexX])*1000), 0);
	buf.writeInt32LE(~~(parseFloat(arrFloats[indexY])*1000), 4);
	buf.writeInt32LE(~~(parseFloat(arrFloats[indexZ])*1000), 8);

	return buf;
}

module.exports = Pdbparser;